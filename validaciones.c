#include<stdio.h>
#include<string.h>
#define SIZE 15

int nombreValido(char *);
int edadValida(int);

int main(){
	int edadProm=0;
	int edadMayor=0;
	int edadMenor=131;
	char personaVieja[SIZE];
	char personaJoven[SIZE];
	int n=10;
	int i=1;
	while(i<=n){
		char nom[SIZE];
		printf("Ingrese su nombre: ");
		scanf("%s", nom);
		if (nombreValido(nom)){
			char ape[SIZE];
			printf("Ingrese su apellido: ");
			scanf("%s", ape);
			while(!nombreValido(ape)){
				printf("Ingrese su apellido: ");
				scanf("%s", ape);
			}
			int ed;			
			printf("Ingrese su edad: ");
			scanf("%d", &ed);
			while(!edadValida(ed)){
				printf("Ingrese su edad: ");
				scanf("%d", &ed);
			}
			edadProm=ed+edadProm;
			if(ed > edadMayor){
				edadMayor=ed;
				strcpy(personaVieja,nom);
				strcat(personaVieja," ");
				strcat(personaVieja,ape);
			}if(ed < edadMenor){
				edadMenor=ed;
				strcpy(personaJoven,nom);
				strcat(personaJoven," ");
				strcat(personaJoven,ape);
			}
			printf("Datos ingresados correctamente\n");
			i++;
		}
	}
	printf("Promedio de edad: %d\n",(edadProm/n));
	printf("Persona mas vieja: %s, edad= %d\nPersona menor: %s, edad= %d \n",personaVieja,edadMayor,personaJoven,edadMenor);
	
}

int nombreValido(char *nombre){
	for(int i=0; i<SIZE; i++){
		char c=nombre[i];
		if(c!=0){
			
			if(i==0 && (c<65 || c>90)){
				printf("El nombre debe empezar con letra en mayuscula \n");
				return 0;
			}
			else if (i!=0 && (c<97 || c>122)){
				printf("El nombre no debe contener caracteres especiales \n");
				return 0;
			}
						
		}

		else
			return 1;		
	}
} 

int edadValida(int edad){
	if (edad<0 || edad>130){
		printf("Edad no válida\n");
		return 0;	
	}
	else	
		return 1;
}
